FROM java:8

ENV VERSION=1.1.0 \
    JAVA_MX=1G


RUN cd home && mkdir otp
ADD http://download.geofabrik.de/europe/netherlands-latest.osm.pbf /home/otp/
ADD http://gtfs.ovapi.nl/new/gtfs-nl.zip /home/otp/

EXPOSE 8080
